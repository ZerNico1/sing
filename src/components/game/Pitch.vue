<script setup lang="ts">
import GameSungPitchNote from '~/components/game/SungPitchNote.vue';
import { getPointsForNoteType, Note } from '~/logic/song/note';
import { Sentence } from '~/logic/song/sentence';
import {
  PitchProcessor,
  ProcessableBeat,
  ProcessedBeat,
} from '~/logic/voice/pitch-processor';
import { Microphone } from '~/stores/settings';

const props = defineProps<{
  currentSentence?: Sentence;
  position: 'top' | 'bottom';
  pitchProcessor?: PitchProcessor;
  delayInBeats: number;
  microphone: Microphone;
}>();

const emit = defineEmits<{
  (e: 'addScore', points: number): void;
}>();

const rowCount = 16;
const noteField = ref<HTMLDivElement>();
const noteFieldSize = useElementSize(noteField);

const rowHeight = computed(() => {
  return noteFieldSize.height.value / rowCount - 1;
});

const columnWidth = computed(() => {
  if (!props.currentSentence) return 0;
  return noteFieldSize.width.value / props.currentSentence.length;
});

const averageMidiNote = computed(() => {
  const sentence = props.currentSentence;
  if (!sentence) return 0;
  const average = Math.round(
    sentence.notes.map((notes) => notes.midiNote).reduce((a, b) => a + b, 0) /
      sentence.notes.length,
  );
  return average;
});

const calculateNoteRow = (midiNote: number): number => {
  let wrappedMidiNote: number = midiNote;

  const minNoteRowMidiNote = Math.floor(averageMidiNote.value - rowCount / 2);
  const maxNoteRowMidiNote = minNoteRowMidiNote + rowCount - 1;

  // move by octave to fit on screen
  while (wrappedMidiNote > maxNoteRowMidiNote && wrappedMidiNote > 0)
    wrappedMidiNote -= 12;

  while (wrappedMidiNote < minNoteRowMidiNote && wrappedMidiNote < 127)
    wrappedMidiNote += 12;

  const offset: number = wrappedMidiNote - averageMidiNote.value;
  let noteRow = Math.ceil(rowCount / 2 + offset);
  noteRow = Math.abs(noteRow - rowCount) - 1;

  return noteRow;
};

// display the sung pitch in the closer octave to the actual midi note for better user experience
const calculateSungNoteRow = (processedBeat: ProcessedBeat) => {
  if (processedBeat.sungNote === processedBeat.note.midiNote)
    return calculateNoteRow(processedBeat.sungNote);

  const noteRow = calculateNoteRow(processedBeat.sungNote);
  const alternativeNoteRow = noteRow - 12;

  // check if alternative note row is inside rows
  if (alternativeNoteRow >= 0 && alternativeNoteRow < rowCount) {
    const correctNoteRow = calculateNoteRow(processedBeat.note.midiNote);

    // check what is closer to correctNoteRow
    if (
      Math.abs(correctNoteRow - noteRow) <
      Math.abs(correctNoteRow - alternativeNoteRow)
    )
      return noteRow;
    else return alternativeNoteRow;
  }

  return calculateNoteRow(processedBeat.sungNote);
};

const calculateGap = (note: Note, prevNote: Note): number => {
  if (!prevNote) return 0;
  return note.startBeat - (prevNote.startBeat + prevNote.length);
};

const sungNoteEls =
  useTemplateRefsList<InstanceType<typeof GameSungPitchNote>>();

const processableBeats: ProcessableBeat[] = [];
const processedBeats = ref<ProcessedBeat[]>([]);

watch(
  () => props.currentSentence,
  (currentSentence) => {
    processedBeats.value.length = 0;

    currentSentence?.notes.forEach((note) => {
      for (let beat = 0; beat < note.length; beat++) {
        const processableBeat: ProcessableBeat = {
          note,
          beat: note.startBeat + beat,
          isFirstBeat: beat === 0,
          isLastBeat: note.startBeat + beat === currentSentence.maxBeat - 1,
        };
        processableBeats.push(processableBeat);
      }
    });
  },
  { immediate: true },
);

watch(
  () => props.pitchProcessor,
  (pitchProcessor) => {
    if (!pitchProcessor) return;
    pitchProcessor.setBeatProcessedCallback(onNoteProcessed);
  },
);

let lastBeatWasValid = false;
const onNoteProcessed = (processedBeat: ProcessedBeat) => {
  if (
    processedBeat.sungNote > 0 &&
    props.currentSentence &&
    processedBeat.beat >= props.currentSentence.minBeat
  ) {
    const prevNote = processedBeats.value.at(-1);
    // extend previous note if new beat is part of same note and the singing was continuous
    if (
      lastBeatWasValid &&
      !processedBeat.isFirstBeat &&
      prevNote &&
      processedBeat.sungNote === prevNote.sungNote
    ) {
      prevNote.length++;
      // else create new displayed note
    } else {
      processedBeats.value.push(processedBeat);
    }
  }
  handleScoring(processedBeat);

  lastBeatWasValid = processedBeat.sungNote > 0;
};

let successBeats = 0;
let totalBeats = 0;

const handleScoring = (processedBeat: ProcessedBeat) => {
  totalBeats++;
  if (
    processedBeat.sungNote > 0 &&
    processedBeat.sungNote === processedBeat.note.midiNote
  ) {
    successBeats++;
    emit('addScore', getPointsForNoteType(processedBeat.note.type));
  }

  if (processedBeat.isLastBeat) {
    if (successBeats / totalBeats >= 0.9) {
      emit('addScore', totalBeats * 1);
    }

    successBeats = 0;
    totalBeats = 0;
  }
};

const calculateGapProcessed = (
  note: ProcessedBeat,
  prevNote: ProcessedBeat,
): number => {
  const prevEnd = prevNote
    ? prevNote.beat + prevNote.length
    : props.currentSentence?.minBeat || 0;
  return note.beat - prevEnd;
};

const update = (currentBeat: number) => {
  // use delayed beat to adjust to microphone delay
  const delayedBeat = currentBeat - props.delayInBeats;

  sungNoteEls.value.forEach((note) => {
    note?.update(delayedBeat);
  });

  if (!props.pitchProcessor) return;

  if (processableBeats.length === 0) return;

  if (delayedBeat > processableBeats[0].beat + 1) {
    const note = processableBeats.shift();
    if (props.pitchProcessor && note) props.pitchProcessor.processBeat(note);
  }
};

defineExpose({
  update,
});
</script>
<template>
  <div
    class="px-12vw"
    :class="[props.position === 'top' ? 'pt-2vh pb-8vh' : 'pt-8vh pb-2vh']"
  >
    <div class="w-full h-full relative">
      <div
        v-if="props.currentSentence"
        ref="noteField"
        class="w-full h-full absolute flex flex-row"
      >
        <GamePitchNote
          v-for="(note, index) in props.currentSentence.notes"
          :key="index"
          :row-height="rowHeight"
          :row="calculateNoteRow(note.midiNote)"
          :column-width="columnWidth"
          :length="note.length"
          :midi-note="note.midiNote"
          :note-type="note.type"
          :gap="
            calculateGap(note, props.currentSentence.notes[index - 1] || null)
          "
        ></GamePitchNote>
      </div>
      <div
        v-if="props.currentSentence"
        class="w-full h-full absolute flex flex-row"
      >
        <GameSungPitchNote
          v-for="(beat, index) in processedBeats"
          :key="index"
          :ref="sungNoteEls.set"
          :microphone="props.microphone"
          :row-height="rowHeight"
          :row="calculateSungNoteRow(beat)"
          :column-width="columnWidth"
          :length="beat.length"
          :midi-note="beat.sungNote"
          :gap="calculateGapProcessed(beat, processedBeats[index - 1] || null)"
        ></GameSungPitchNote>
      </div>
    </div>
  </div>
</template>
