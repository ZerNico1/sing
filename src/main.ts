import { createHead } from '@vueuse/head';
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';
import routes from 'virtual:generated-pages';
import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

import App from './App.vue';

import '@unocss/reset/tailwind.css';
import './styles/main.css';
import 'uno.css';
import { loadGuards } from './router-guards';

const app = createApp(App);
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

loadGuards(router);

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

const head = createHead();

app.use(head);
app.use(pinia);
app.use(router);
app.mount('#app');
