import { Song } from '../song/song';

export function millisecondInSongToBeat(
  song: Song,
  millisInSong: number,
): number {
  return millisecondInSongToBeatWithoutGap(song, millisInSong - song.gap);
}

export function millisecondInSongToBeatWithoutGap(
  song: Song,
  millisInSong: number,
): number {
  const beatsPerMinute = getBeatsPerMinute(song);
  const result = (beatsPerMinute * millisInSong) / 1000.0 / 60.0;
  return result;
}

export function beatToMillisecondsInSong(song: Song, beat: number): number {
  return beatToMillisecondsInSongWithoutGap(song, beat) + song.gap;
}

export function beatToMillisecondsInSongWithoutGap(
  song: Song,
  beat: number,
): number {
  const beatsPerMinute = getBeatsPerMinute(song);
  const millisecondsPerBeat = 60000.0 / beatsPerMinute;
  const millisecondsInSong = beat * millisecondsPerBeat;
  return millisecondsInSong;
}

export function getBeatsPerMinute(song: Song): number {
  // Multiply by 4 because UltraStar songs use bars per minute instead of beats per minute
  return song.bpm * 4;
}
