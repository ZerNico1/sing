import { defineStore } from 'pinia';

import { Song } from '~/logic/song/song';
interface LocalSource {
  name: string;
  songs: Song[];
}
export interface SongState {
  localSources: LocalSource[];
}

export const useSongStore = defineStore('songs', {
  state: (): SongState => ({
    localSources: [],
  }),
  actions: {
    addLocalSource(name: string, songs: Song[]) {
      this.localSources.push({ name, songs });
    },
    deleteLocalSource(index: number) {
      this.localSources.splice(index, 1);
    },
  },
  getters: {
    getSongs: (state: SongState) => {
      // get all songs with unique hash
      const localSongsMap = new Map<string, Song>();
      for (const source of state.localSources) {
        for (const song of source.songs) {
          localSongsMap.set(song.hash, song);
        }
      }

      const localSongs = Array.from(localSongsMap.values());
      return localSongs;
    },
  },
});
