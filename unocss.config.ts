import { defineConfig, presetUno } from 'unocss';

export default defineConfig({
  presets: [presetUno()],
  rules: [
    [
      /^animate-spin-(\d+)$/,
      ([, d]) => ({ animation: `spin ${d}ms linear infinite` }),
    ],
  ],
});
