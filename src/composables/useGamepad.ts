export enum GamepadButtonName {
  A = 'A',
  B = 'B',
  X = 'X',
  Y = 'Y',
  LB = 'LB',
  RB = 'RB',
  LT = 'LT',
  RT = 'RT',
  BACK = 'BACK',
  START = 'START',
  LS = 'LS',
  RS = 'RS',
  DPAD_UP = 'DPAD_UP',
  DPAD_DOWN = 'DPAD_DOWN',
  DPAD_LEFT = 'DPAD_LEFT',
  DPAD_RIGHT = 'DPAD_RIGHT',
  XBOX = 'XBOX',
  L_AXIS_X = 'L_AXIS_X',
  L_AXIS_Y = 'L_AXIS_Y',
  R_AXIS_X = 'R_AXIS_X',
  R_AXIS_Y = 'R_AXIS_Y',
  UNKNOWN = 'UNKNOWN',
}

export interface GamepadButtonEvent {
  button: GamepadButtonName;
  repeat: boolean;
  gamepadId: number;
  direction: number;
}

export default function useGamepad(
  callback: (e: GamepadButtonEvent) => void,
  repeatTimeout: number,
  axisThreshold: number,
) {
  const buttonHistories = new Map<number, Map<number, number>>();
  const axesHistories = new Map<number, Map<number, number>>();

  let looping = false;
  let isInitialPress = true;

  useEventListener('gamepadconnected', (e: GamepadEvent) => {
    buttonHistories.set(e.gamepad.index, new Map());
    axesHistories.set(e.gamepad.index, new Map());
  });

  useEventListener('gamepaddisconnected', (e: GamepadEvent) => {
    buttonHistories.delete(e.gamepad.index);
    axesHistories.delete(e.gamepad.index);
  });

  onMounted(() => {
    const gamepads = navigator.getGamepads();

    for (const gamepad of gamepads) {
      if (gamepad) {
        buttonHistories.set(gamepad.index, new Map());
        axesHistories.set(gamepad.index, new Map());
      }
    }
  });

  // request animation frame loop
  const update = () => {
    const gamepads = navigator.getGamepads();
    const currentTime = new Date().getTime();

    // loop through all gamepads
    for (const gamepad of gamepads) {
      if (!gamepad) continue;
      const buttonHistory = buttonHistories.get(gamepad.index);
      const axesHistory = axesHistories.get(gamepad.index);
      if (!buttonHistory || !axesHistory) continue;
      let pressed = false;

      for (const [index, button] of gamepad.buttons.entries()) {
        // Sets repeat to true if button is held
        if (button.pressed) {
          pressed = true;
          if (isInitialPress) continue;
          const lastPress = buttonHistory.get(index);
          if (!lastPress) {
            buttonHistory?.set(index, currentTime);
            sendButtonEvent(gamepad.index, index, false);
          } else if (currentTime - lastPress > repeatTimeout) {
            sendButtonEvent(gamepad.index, index, true);
          }
        } else {
          buttonHistory?.delete(index);
        }
      }
      if (!pressed) isInitialPress = false;
      for (const [index, axis] of gamepad.axes.entries()) {
        if (Math.abs(axis) > axisThreshold) {
          const lastPress = axesHistory.get(index);
          if (!lastPress) {
            axesHistory?.set(index, currentTime);
            sendAxisEvent(gamepad.index, index, false, axis);
          } else if (currentTime - lastPress > repeatTimeout) {
            sendAxisEvent(gamepad.index, index, true, axis);
          }
        } else {
          axesHistory?.delete(index);
        }
      }
    }
  };

  const loop = () => {
    if (!looping) return;
    update();
    requestAnimationFrame(loop);
  };

  const sendButtonEvent = (
    gamepadId: number,
    buttonCode: number,
    isRepeat: boolean,
  ) => {
    const event: GamepadButtonEvent = {
      repeat: isRepeat,
      button: gamepadButtons.get(buttonCode) || GamepadButtonName.UNKNOWN,
      gamepadId,
      direction: 0,
    };

    callback(event);
  };

  const sendAxisEvent = (
    gamepadId: number,
    axisCode: number,
    isRepeat: boolean,
    direction: number,
  ) => {
    const event: GamepadButtonEvent = {
      repeat: isRepeat,
      button: gamepadAxes.get(axisCode) || GamepadButtonName.UNKNOWN,
      gamepadId,
      direction,
    };

    callback(event);
  };

  const startLoop = () => {
    looping = true;
    loop();
  };

  const stopLoop = () => {
    looping = false;
  };

  return { startLoop, stopLoop, update };
}

const gamepadButtons = new Map<number, GamepadButtonName>([
  [0, GamepadButtonName.A],
  [1, GamepadButtonName.B],
  [2, GamepadButtonName.X],
  [3, GamepadButtonName.Y],
  [4, GamepadButtonName.LB],
  [5, GamepadButtonName.RB],
  [6, GamepadButtonName.LT],
  [7, GamepadButtonName.RT],
  [8, GamepadButtonName.BACK],
  [9, GamepadButtonName.START],
  [10, GamepadButtonName.LS],
  [11, GamepadButtonName.RS],
  [12, GamepadButtonName.DPAD_UP],
  [13, GamepadButtonName.DPAD_DOWN],
  [14, GamepadButtonName.DPAD_LEFT],
  [15, GamepadButtonName.DPAD_RIGHT],
  [16, GamepadButtonName.XBOX],
]);

const gamepadAxes = new Map<number, GamepadButtonName>([
  [0, GamepadButtonName.L_AXIS_X],
  [1, GamepadButtonName.L_AXIS_Y],
  [2, GamepadButtonName.R_AXIS_X],
  [3, GamepadButtonName.R_AXIS_Y],
]);
