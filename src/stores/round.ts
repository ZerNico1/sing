import { defineStore } from 'pinia';

import { Song } from '~/logic/song/song';
interface RoundState {
  song?: Song;
  scores?: Map<number, number>;
}

export const useRoundStore = defineStore('round', {
  state: (): RoundState => ({
    song: undefined,
    scores: undefined,
  }),
  actions: {
    setSong(song: Song) {
      this.song = song;
    },
    addScore(index: number, points: number) {
      if (!this.scores) return;

      const score = this.scores.get(index);

      if (score !== undefined) {
        this.scores.set(index, score + points);
      }
    },
    initRound({ playerCount }: { playerCount: number }) {
      this.scores = new Map();
      for (let i = 0; i < playerCount; i++) {
        this.scores.set(i, 0);
      }
    },
  },
  getters: {
    getSong(state) {
      return state.song;
    },
  },
});
