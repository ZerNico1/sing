import { Note } from '../song/note';
import { frequencyToMidi } from '../utils/midi.utils';

export interface ProcessableBeat {
  note: Note;
  isFirstBeat: boolean;
  isLastBeat: boolean;
  beat: number;
}

export interface ProcessedBeat extends ProcessableBeat {
  sungNote: number;
  length: number;
}

export class PitchProcessor {
  private audioWorkletNode: AudioWorkletNode | undefined;
  private samplesPerBeat: number;
  private beatProcessedCallback?: (processedNote: ProcessedBeat) => void;
  beatQueue: ProcessableBeat[] = [];
  private hasJoker = true;

  constructor(
    audioWorkletNode: AudioWorkletNode | undefined,
    samplesPerBeat: number,
  ) {
    this.audioWorkletNode = audioWorkletNode;
    this.samplesPerBeat = samplesPerBeat;
    if (this.audioWorkletNode) {
      this.audioWorkletNode.port.onmessage = this.onBeatProcessed.bind(this);
    }
  }

  processBeat(processableBeat: ProcessableBeat) {
    if (!this.audioWorkletNode) return;
    this.beatQueue.push(processableBeat);

    // Get pitch from audio worklet
    this.audioWorkletNode.port.postMessage({
      type: 'pitch',
      data: this.samplesPerBeat,
    });
  }

  onBeatProcessed(message: MessageEvent<any>) {
    const sungFrequency = message.data.message;
    const processableBeat = this.beatQueue.shift();
    if (!processableBeat) return;

    const sungMidiNote = frequencyToMidi(sungFrequency);

    let adjustedMidiNote = this.applyProcessing(sungMidiNote, processableBeat);

    adjustedMidiNote = this.applyJokerRule(adjustedMidiNote, processableBeat);

    const processedBeat: ProcessedBeat = {
      ...processableBeat,
      sungNote: adjustedMidiNote,
      length: 1,
    };

    if (this.beatProcessedCallback) {
      this.beatProcessedCallback(processedBeat);
    }
  }

  // Adjust sung note to the needed note if it's close enough
  applyProcessing(note: number, processableBeat: ProcessableBeat) {
    const midiNote = processableBeat.note.midiNote;
    if (note > 0) {
      let midiDifference = Math.abs(note - processableBeat.note.midiNote);
      midiDifference %= 12;

      if (midiDifference > 6) {
        midiDifference = 12 - midiDifference;
      }
      if (midiDifference < 2.5) {
        return midiNote;
      }
    }
    return Math.round(note);
  }

  // Pitch detection isn't perfec so grant user a joker
  applyJokerRule(note: number, processableBeat: ProcessableBeat) {
    const midiNote = processableBeat.note.midiNote;

    if (note === 0 && this.hasJoker) {
      this.hasJoker = false;
      return midiNote;
    }
    this.hasJoker = note === midiNote;
    return note;
  }

  setBeatProcessedCallback(callback: (processedNote: ProcessedBeat) => void) {
    this.beatProcessedCallback = callback;
  }
}
