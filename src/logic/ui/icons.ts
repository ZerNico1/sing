import ChevronLeft from '~/assets/icons/chevron-left.svg?component';
import Dice from '~/assets/icons/dice.svg?component';
import Duet from '~/assets/icons/duet.svg?component';
import Folder from '~/assets/icons/folder.svg?component';
import Gear from '~/assets/icons/gear.svg?component';
import Group from '~/assets/icons/group.svg?component';
import HighVolume from '~/assets/icons/high-volume.svg?component';
import Info from '~/assets/icons/info.svg?component';
import KeyArrows from '~/assets/icons/keys/key-arrows.svg?component';
import KeyEnter from '~/assets/icons/keys/key-enter.svg?component';
import KeyEscape from '~/assets/icons/keys/key-escape.svg?component';
import KeyF3 from '~/assets/icons/keys/key-f3.svg?component';
import KeyF4 from '~/assets/icons/keys/key-f4.svg?component';
import KeyPageDown from '~/assets/icons/keys/key-page-down.svg?component';
import KeyPageUp from '~/assets/icons/keys/key-page-up.svg?component';
import XboxA from '~/assets/icons/keys/xbox-a.svg?component';
import XboxB from '~/assets/icons/keys/xbox-b.svg?component';
import XboxCross from '~/assets/icons/keys/xbox-cross.svg?component';
import XboxLB from '~/assets/icons/keys/xbox-lb.svg?component';
import XboxMenu from '~/assets/icons/keys/xbox-menu.svg?component';
import XboxRB from '~/assets/icons/keys/xbox-rb.svg?component';
import XboxY from '~/assets/icons/keys/xbox-y.svg?component';
import LowVolume from '~/assets/icons/low-volume.svg?component';
import Microphone from '~/assets/icons/microphone.svg?component';
import Minus from '~/assets/icons/minus.svg?component';
import MusicFolder from '~/assets/icons/music-folder.svg?component';
import Person from '~/assets/icons/person.svg?component';
import Plus from '~/assets/icons/plus.svg?component';
import Search from '~/assets/icons/search.svg?component';
import Spinner from '~/assets/icons/spinner.svg?component';
import TriangleArrow from '~/assets/icons/triangle-arrow.svg?component';
import VinylRecord from '~/assets/icons/vinyl-record.svg?component';

export {
  KeyArrows,
  KeyEnter,
  KeyEscape,
  Person,
  XboxA,
  XboxB,
  XboxCross,
  Gear,
  Group,
  Info,
  Microphone,
  MusicFolder,
  VinylRecord,
  Dice,
  ChevronLeft,
  Folder,
  Plus,
  Spinner,
  HighVolume,
  LowVolume,
  TriangleArrow,
  XboxLB,
  XboxRB,
  KeyPageDown,
  KeyPageUp,
  Minus,
  XboxMenu,
  Search,
  KeyF3,
  Duet,
  KeyF4,
  XboxY,
};
