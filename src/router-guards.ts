import { Router } from 'vue-router';

import { initialized } from './logic/ui/init';
export const loadGuards = (router: Router) => {
  router.beforeEach((to, from, next) => {
    if (!initialized.value) {
      initialized.value = true;
      next({
        path: '/',
      });
    } else {
      next();
    }
  });

  router.beforeEach((to, from, next) => {
    if (to.path === '/sing' && from.path === '/sing/results') {
      next({
        path: '/songs',
      });
    } else if (to.path === '/sing/results' && from.path === '/songs') {
      next({
        path: '/',
      });
    } else {
      next();
    }
  });
};
