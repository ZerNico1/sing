import { Md5 } from 'ts-md5';

import { Note, NoteType } from './note';
import { Sentence } from './sentence';
import { LocalSong, Song } from './song';
import { Voice } from './voice';

export class ParseError extends Error {
  constructor(msg: string) {
    super(msg);
    Object.setPrototypeOf(this, ParseError.prototype);
  }
}

export const parseLocalSongfile = async (
  songHandle: FileSystemFileHandle,
  files: (FileSystemDirectoryHandle | FileSystemFileHandle)[],
) => {
  const file = await songHandle.getFile();
  const fileTxt = await file.text();
  try {
    const songData = parseSongTxt(fileTxt);

    const fileHandles = files.filter(
      (file) => file.kind === 'file',
    ) as FileSystemFileHandle[];

    let videoHandle;
    if (songData.video) {
      videoHandle = fileHandles.find((file) => file.name === songData.video);
    }

    let mp3Handle;
    if (songData.mp3) {
      mp3Handle = fileHandles.find((file) => file.name === songData.mp3);
    }
    if (!mp3Handle) {
      throw new ParseError('No mp3 file found');
    }

    let coverHandle;
    if (songData.cover) {
      coverHandle = fileHandles.find((file) => file.name === songData.cover);
    }

    let backgroundHandle;
    if (songData.background) {
      backgroundHandle = fileHandles.find(
        (file) => file.name === songData.background,
      );
    }

    const song = new LocalSong(songData, {
      video: videoHandle,
      mp3: mp3Handle,
      cover: coverHandle,
      background: backgroundHandle,
    });

    return song;
  } catch (e: any | Error | ParseError) {
    if (e instanceof ParseError) {
      console.error(`Could not parse ${songHandle.name}`, e.message);
    } else if (e instanceof Error) {
      console.error(`Could not parse ${songHandle.name}`, e.message);
    } else {
      console.error(`Could not parse ${songHandle.name}`, 'Unknown error');
    }
  }
};

export const parseSongTxt = (fileTxt: string) => {
  const lines: string[] = fileTxt.split(/\r?\n/);

  let title: string | undefined;
  let artist: string | undefined;
  let language: string | undefined;
  let edition: string | undefined;
  let genre: string | undefined;
  let year: number | undefined;
  let bpm: number | undefined;
  let gap = 0;
  let mp3: string | undefined;
  let cover: string | undefined;
  let video: string | undefined;
  let background: string | undefined;
  let relative = false;
  let hash: string | undefined;
  let videoGap = 0;

  let notes: Note[] = [];
  let sentences: Sentence[] = [];
  const voices: Voice[] = [];
  const md5 = new Md5();

  lines.forEach((line) => {
    if (line === '') return;
    if (line.startsWith('#')) {
      const [property, value]: string[] = line
        .substring(1, line.length)
        .trim()
        .split(':');

      switch (property.toLowerCase()) {
        case 'title':
          title = value;
          break;
        case 'artist':
          artist = value;
          break;
        case 'language':
          language = value;
          break;
        case 'edition':
          edition = value;
          break;
        case 'genre':
          genre = value;
          break;
        case 'year':
          year = parseInt(value, 10);
          break;
        case 'bpm':
          bpm = parseFloat(value.replace(',', '.'));
          break;
        case 'gap':
          gap = parseFloat(value.replace(',', '.'));
          break;
        case 'mp3':
          mp3 = value;
          break;
        case 'cover':
          cover = value;
          break;
        case 'video':
          video = value;
          break;
        case 'background':
          background = value;
          break;
        case 'relative':
          relative = value === 'true';
          break;
        case 'videogap':
          videoGap = parseFloat(value.replace(',', '.'));
      }
    } else if ([':', '*', 'F', 'R', 'G'].includes(line.charAt(0))) {
      const [tag, startBeat, length, txtPitch, ...text]: string[] = line
        .substring(0, line.length)
        .split(' ');

      const note: Note = new Note(
        tagToNoteType(tag),
        parseInt(startBeat),
        parseInt(length),
        parseInt(txtPitch),
        text.join(' '),
      );
      notes.push(note);
      md5.appendStr(line);
    } else if (line.charAt(0) === '-') {
      const [, linebreakBeat]: string[] = line
        .substring(1, line.length)
        .split(' ');
      sentences.push(new Sentence(notes, parseInt(linebreakBeat)));
      notes = [];
      md5.appendStr(line);
    } else if (line.charAt(0) === 'P') {
      if (sentences.length === 0) return;
      const lastNote: Note = notes[notes.length - 1];
      if (notes.length !== 0) {
        sentences.push(
          new Sentence(notes, lastNote.startBeat + lastNote.length + 1),
        );
      }
      voices.push(new Voice(sentences));
      notes = [];
      sentences = [];
    } else if (line.charAt(0) === 'E') {
      if (notes.length > 0) {
        const lastNote: Note = notes[notes.length - 1];
        sentences.push(
          new Sentence(notes, lastNote.startBeat + lastNote.length + 1),
        );
      }

      notes = [];
      voices.push(new Voice(sentences));

      hash = md5.end().toString();
    }
  });

  if (!title) throw new ParseError('No title');
  if (!artist) throw new ParseError('No artist');
  if (!bpm) throw new ParseError('No bpm');
  if (!hash) throw new ParseError('No hash');
  if (voices.length === 0) throw new ParseError('No voices');

  return {
    title,
    artist,
    language,
    edition,
    genre,
    year,
    bpm,
    gap,
    mp3,
    cover,
    video,
    background,
    relative,
    hash,
    videoGap,
    voices,
  };
};

function tagToNoteType(tag: string): NoteType {
  switch (tag) {
    case ':':
      return NoteType.Normal;
    case '*':
      return NoteType.Golden;
    case 'F':
      return NoteType.Freestyle;
    case 'R':
      return NoteType.Rap;
    case 'G':
      return NoteType.RapGolden;
  }
  return NoteType.Normal;
}

export const searchSongs = async (folder: FileSystemDirectoryHandle) => {
  const songs: Song[] = [];

  const recursiveSearchSongs = async (folder: FileSystemDirectoryHandle) => {
    const files: (FileSystemDirectoryHandle | FileSystemFileHandle)[] = [];
    for await (const entry of folder.values()) {
      files.push(entry);
    }

    for (const file of files) {
      if (file.kind === 'file') {
        if (file.name.endsWith('.txt')) {
          const song = await parseLocalSongfile(file, files);
          if (song) {
            songs.push(song);
          }
        }
      } else if (file.kind === 'directory') {
        await recursiveSearchSongs(file);
      }
    }
  };

  await recursiveSearchSongs(folder);

  return songs;
};
