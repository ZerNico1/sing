import { defineStore } from 'pinia';

export interface Microphone {
  label: string;
  id: string;
  channel: number;
  gain: number;
  color: string;
  delay: number;
}

interface SettingsState {
  volume: {
    master: number;
    game: number;
    preview: number;
  };
  microphones: Microphone[];
}

export const useSettingsStore = defineStore('settings', {
  state: (): SettingsState => ({
    volume: {
      master: 100,
      game: 100,
      preview: 50,
    },
    microphones: [],
  }),
  getters: {
    getPreviewVolume: (state: SettingsState): number => {
      // reduce preview volume by master volume
      return state.volume.preview * (state.volume.master / 100);
    },
    getGameVolume: (state: SettingsState): number => {
      // reduce game volume by master volume
      return state.volume.game * (state.volume.master / 100);
    },
  },
  actions: {
    setMicrophone(index: number, microphone: Microphone) {
      this.microphones[index] = microphone;
    },
    deleteMicrophone(index: number) {
      this.microphones.splice(index, 1);
    },
  },
  persist: true,
});
