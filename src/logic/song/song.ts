import { Voice } from './voice';

export type Song2 = any;

export interface Song {
  title: string;
  artist: string;
  language?: string;
  edition?: string;
  genre?: string;
  year?: number;
  bpm: number;
  gap: number;
  relative: boolean;
  voices: Voice[];
  hash: string;
  videoGap: number;

  getVideoUrl(): Promise<string | undefined>;
  getMp3Url(): Promise<string | undefined>;
  getCoverUrl(): Promise<string | undefined>;
  getBackgroundUrl(): Promise<string | undefined>;

  clearCoverUrl(): void;
  clearBackgroundUrl(): void;
  clearVideoUrl(): void;
  clearMp3Url(): void;
}

export class LocalSong implements Song {
  title: string;
  artist: string;
  language?: string;
  edition?: string;
  genre?: string;
  year?: number;
  bpm: number;
  gap: number;
  relative: boolean;
  voices: Voice[];
  hash: string;
  videoGap: number;

  private videoHandle?: FileSystemFileHandle;
  private mp3Handle: FileSystemFileHandle;
  private coverHandle?: FileSystemFileHandle;
  private backgroundHandle?: FileSystemFileHandle;

  private cachedCoverUrl?: string = undefined;
  private cachedBackgroundUrl?: string = undefined;
  private cachedMp3Url?: string = undefined;
  private cachedVideoUrl?: string = undefined;

  constructor(
    song: {
      title: string;
      artist: string;
      language?: string;
      edition?: string;
      genre?: string;
      year?: number;
      bpm: number;
      gap: number;
      relative: boolean;
      voices: Voice[];
      hash: string;
      videoGap: number;
    },
    fileHandles: {
      video?: FileSystemFileHandle;
      mp3: FileSystemFileHandle;
      cover?: FileSystemFileHandle;
      background?: FileSystemFileHandle;
    },
  ) {
    this.title = song.title;
    this.artist = song.artist;
    this.language = song.language;
    this.edition = song.edition;
    this.genre = song.genre;
    this.year = song.year;
    this.bpm = song.bpm;
    this.gap = song.gap;
    this.relative = song.relative;
    this.voices = song.voices;
    this.hash = song.hash;
    this.videoGap = song.videoGap;

    this.videoHandle = fileHandles.video;
    this.mp3Handle = fileHandles.mp3;
    this.coverHandle = fileHandles.cover;
    this.backgroundHandle = fileHandles.background;
  }

  async getVideoUrl() {
    if (!this.videoHandle) return undefined;
    if (this.cachedVideoUrl) return this.cachedVideoUrl;

    this.cachedVideoUrl = URL.createObjectURL(await this.videoHandle.getFile());
    return this.cachedVideoUrl;
  }

  clearBackgroundUrl() {
    if (!this.cachedBackgroundUrl) return;
    URL.revokeObjectURL(this.cachedBackgroundUrl);
    this.cachedBackgroundUrl = undefined;
  }

  async getMp3Url() {
    if (!this.mp3Handle) return undefined;
    if (this.cachedMp3Url) return this.cachedMp3Url;

    this.cachedMp3Url = URL.createObjectURL(await this.mp3Handle.getFile());
    return this.cachedMp3Url;
  }

  clearMp3Url() {
    if (!this.cachedMp3Url) return;
    URL.revokeObjectURL(this.cachedMp3Url);
    this.cachedMp3Url = undefined;
  }

  async getCoverUrl() {
    if (!this.coverHandle) return undefined;
    if (this.cachedCoverUrl) return this.cachedCoverUrl;

    this.cachedCoverUrl = URL.createObjectURL(await this.coverHandle.getFile());
    return this.cachedCoverUrl;
  }

  clearCoverUrl() {
    if (!this.cachedCoverUrl) return;
    URL.revokeObjectURL(this.cachedCoverUrl);
    this.cachedCoverUrl = undefined;
  }

  async getBackgroundUrl() {
    if (!this.backgroundHandle) return undefined;
    if (this.cachedBackgroundUrl) return this.cachedBackgroundUrl;

    this.cachedBackgroundUrl = URL.createObjectURL(
      await this.backgroundHandle.getFile(),
    );
    return this.cachedBackgroundUrl;
  }

  clearVideoUrl() {
    if (!this.cachedVideoUrl) return;
    URL.revokeObjectURL(this.cachedVideoUrl);
    this.cachedVideoUrl = undefined;
  }
}
