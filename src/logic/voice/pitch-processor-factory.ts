import pitchWasm from 'dywapitchtrack/dywapitchtrack_bg.wasm?url';
import pitchWorklet from 'pitch-detection-audio-worklet/dist/pitch.worklet.js?url';

import { Song } from '../song/song';
import { beatToMillisecondsInSongWithoutGap } from '../utils/bpm.utils';

import { PitchProcessor } from './pitch-processor';

import { Microphone } from '~/stores/settings';

export class PitchProcessorFactory {
  map = new Map<string, MediaStream>();
  audioCtx = new AudioContext({ sampleRate: 48000 });

  async init() {
    await this.audioCtx.audioWorklet.addModule(pitchWorklet);
  }

  async createPitchProcessor(
    mic: Microphone,
    song: Song,
  ): Promise<PitchProcessor> {
    const stream = await this.getMediaStream(mic);
    let audioWorklet: AudioWorkletNode | undefined;

    const samplesPerBeat = Math.floor(
      (48000 * beatToMillisecondsInSongWithoutGap(song, 1)) / 1000,
    );

    if (stream) {
      const source = this.audioCtx.createMediaStreamSource(stream);

      // seperate audio channels
      const splitter = this.audioCtx.createChannelSplitter(8);
      source.connect(splitter);

      // gain node
      const gainNode = this.audioCtx.createGain();
      gainNode.gain.value = mic.gain;
      splitter.connect(gainNode, mic.channel - 1, 0);

      audioWorklet = await this.constructWorklet(gainNode);
    }

    return new PitchProcessor(audioWorklet, samplesPerBeat);
  }

  async getMediaStream(mic: Microphone): Promise<MediaStream | undefined> {
    // store stream because you can only create 1 per device
    let stream = this.map.get(mic.id);
    if (stream) {
      return stream;
    }

    try {
      // disable browser optimizations for audio stream (breaks stereo channels)
      stream = await navigator.mediaDevices.getUserMedia({
        audio: {
          deviceId: { exact: mic.id },
          echoCancellation: false,
          autoGainControl: false,
          noiseSuppression: false,
        },
      });
      this.map.set(mic.id, stream);
      return stream;
    } catch (error) {
      console.error(error);
    }
    return undefined;
  }

  async constructWorklet(node: AudioNode): Promise<AudioWorkletNode> {
    const worklet = new AudioWorkletNode(this.audioCtx, 'pitch-worklet');
    node.connect(worklet);
    // Send web assembly buffer to worklet
    const wasmResponse = await fetch(pitchWasm);
    const wasmBuffer = await wasmResponse.arrayBuffer();
    worklet.port.postMessage({ type: 'init', data: wasmBuffer }, [wasmBuffer]);
    return worklet;
  }

  async stopStreams() {
    this.map.forEach((stream) => {
      stream.getTracks().forEach((track) => {
        track.stop();
      });
    });
    await this.audioCtx.close();
  }
}
